# Déploiement automatique avec Gitlab CI/CD

Ce repo est une démonstration des possibilités de gitlab CI/CD dans le cadre de Rennes DevOps.

- Etape 1 : Enregistrement du runner
- Etape 2 : Déploiement d'un site web
- Etape 3 : Mise à jour de la production manuellement
- Etape 4 : Mise à jour de la prod automatiquement avec tests unitaires


Pour commencer, **forkez** ce projet puis placez-vous sur la branche E1 de votre fork et suivez le fichier README.md

Note : 
- Virtualbox + Vagrant + Ansible sont utilisés pour l'environnement de démonstration.
- L'image "ubuntu/xenial64" est utilisée par Vagrant.
